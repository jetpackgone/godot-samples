# Select/Hover over top level Area2D only

A common scenario in games with mouse controls is to click or hover over only the top level game object in order to interact with it. This project showcases a system where the player can do so based on mouse events captured by dynamically instantiated Area2D nodes and a custom y-sort on detected objects.

Created in Godot v3.4.4

## How to Run

Download and run [Godot game engine](https://godotengine.org/download).

Open the `project.godot` in Godot and press `F5` to play the sample game.

## The Problem

All overlapping Area2D nodes will handle input at a given mouse position, but the player/dev often wants to only have the top level Area2D handle input.

The first approach is often to use a YSort node which lets Godot automatically update the z-index where nodes are rendered. While this does render objects as expected, input is not guaranteed to be handled in the same order as the rendering. Input is handled by the order of the nodes in the tree, which does not always match the rendering order.

Another approach could be to try using Control nodes instead of Area2D nodes. This results in the same issue where input handling order does not match rendering order.

## The Solution

Since Godot doesn't have built-in y-sorted input handling, we'll have to implement it ourselves. We can do this by tracking game objects in an array in an `ObjectManager` singleton. Since tracking all game objects may not scale, the array only track objects detected by Area2Ds. Whenever an object is added or removed from from the array, the array is sorted by `position.y` descending so that the object at index 0 always has the highest y position, which is equivalent to the top level object.

![Demo](./area2d_top_demo.gif)