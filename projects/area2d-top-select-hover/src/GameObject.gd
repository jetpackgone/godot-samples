extends Sprite

var mouse_offset := Vector2.ZERO

func _ready() -> void:
	$IndexLabel.text = str(get_index())

func _process(delta: float) -> void:
	if ObjectManager.selected_object == self:
		global_position = get_global_mouse_position() + mouse_offset

func _on_Area2D_mouse_entered() -> void:
	ObjectManager.add_highlighted_object(self)

func _on_Area2D_mouse_exited() -> void:
	ObjectManager.remove_highlighted_object(self)

func _on_Area2D_input_event(viewport: Node, event: InputEvent, shape_idx: int) -> void:
	if event is InputEventMouseButton && event.button_index == BUTTON_LEFT:
		if event.pressed:
			mouse_offset = global_position - get_global_mouse_position()
			ObjectManager.select_object(self)
		else:
			ObjectManager.deselect_object()

func highlight():
	$Label.text = "Click to select!"
	modulate = Color("#5fff00")

func unhighlight():
	_reset()

func select():
	$Label.text = "Selected!"
	modulate = Color("#ff37f7")

func deselect():
	_reset()

func _reset():
	$Label.text = "Click and drag me!"
	modulate = Color("#ffffff")
