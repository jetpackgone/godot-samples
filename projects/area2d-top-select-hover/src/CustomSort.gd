class_name CustomSort

static func y_sort_descending(a, b) -> bool:
	if a == null || b == null || a.position == null || b.position == null:
		return false
	return a.position.y > b.position.y
