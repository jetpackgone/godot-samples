extends Node

signal highlighted_objects_updated(objects) # Notifies UI for debugging purposes

var selected_object # Currently selected object, null if none selected
var objects = [] # Array of detected objects
var mutex: Mutex = Mutex.new() # Used to prevent race conditions whenever objects array is accessed

# Add new game object to array and highlight the top level object
func add_highlighted_object(new_object):
	mutex.lock()
	if new_object != null:
		# Add given object to array if not yet added
		if !objects.has(new_object):
			objects.append(new_object)
		
		if objects.size() > 0:
			# Re-sort the array
			_sort()
			
			# Highlight the object at index 0 (top level) and unhighlight the rest
			_highlight_top()
		
		emit_signal("highlighted_objects_updated", objects)
	mutex.unlock()

# Remove given game object to array and highlight the top level object
func remove_highlighted_object(new_object):
	mutex.lock()
	if new_object != null:
		# Find and remove object from array if found
		var i: int = objects.find(new_object)
		if i > -1:
			objects.remove(i)
			
			# Unhighlight the removed object unless currently selected
			if selected_object != new_object:
				new_object.unhighlight()
			
			if objects.size() > 0:
				# Re-sort the array
				_sort()
				# Highlight the object at index 0 (top level) and unhighlight the rest
				_highlight_top()
			
			emit_signal("highlighted_objects_updated", objects)
	mutex.unlock()

# Store the top level object as selected and call its select() method
func select_object(new_object):
	mutex.lock()
	if objects.size() > 0:
		# Deselect the currently selected object unless same as the one we want to select
		if selected_object != null && selected_object != objects[0]:
			selected_object.deselect()
		
		# Set top level object as selected
		selected_object = objects[0]
		selected_object.select()
	mutex.unlock()

#  Clear the selected object, call its deselect() method, and highlight the top level object
func deselect_object():
	mutex.lock()
	if selected_object != null:
		selected_object.deselect()
		selected_object = null
		
		# Sort and highlight the top level object after deselecting
		if objects.size() > 0:
			_sort()
			_highlight_top()
		
		emit_signal("highlighted_objects_updated", objects)
	mutex.unlock()

# Highlight the object at index 0 (top) and unhighlight the rest
func _highlight_top():
	if selected_object == null:
		for j in range(objects.size()):
			if j == 0:
				objects[j].highlight()
			else:
				objects[j].unhighlight()

# Sorts the array by calling a custom sort that orders by y position descending
# See CustomSort class for more
func _sort():
	objects.sort_custom(CustomSort, "y_sort_descending")
