extends Node2D

func _ready() -> void:
	ObjectManager.connect("highlighted_objects_updated", self, "_on_highlighted_objects_updated")

func _on_Button_pressed() -> void:
	var new_area = load("res://GameObject.tscn").instance()
	$YSort.add_child(new_area)
	new_area.position = $SpawnPosition.position

func _on_highlighted_objects_updated(highlighted_objects: Array):
	var result = ""
	for i in range(highlighted_objects.size()):
		if i == 0:
			result += str(highlighted_objects[i].get_index())
		else:
			result += ", " + str(highlighted_objects[i].get_index())
	$CanvasLayer/Panel/H/V/HighlightedLabel.text = result
