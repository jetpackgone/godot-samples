# Godot Samples

A collection of sample Godot projects that showcase my solutions to use cases I've encountered. Free and open source for everyone to learn from!

## Sample Projects

- [Select/Hover over top level Area2D only](https://gitlab.com/jetpackgone/godot-samples/-/tree/main/projects/area2d-top-select-hover)

## License

MIT License

Free to copy and share! Feel free to give me credit, but not required!
